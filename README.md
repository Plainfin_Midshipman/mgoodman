## Maurice Goodman's Personal R Package

This package contains functions I use often. Some of these functions, such as `av_plot` and `residuals_plots` are useful for creating attractive residuals plots using ggplot2, while others, such as `convert_urchin_data` are for specific research projects and require data frames structured in specific ways. It can be installed using the `devtools` package with the following code:

```
install.packages("devtools")

devtools::install_bitbucket("Plainfin_Midshipman/mgoodman")
```

This package depends on the following R packages, and will install them when installed: **ggplot2**, **readxl**, **plyr**, **grepel**, and **lmerTest**.

## License

This package is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License, version 2, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchantability or fitness for a particular purpose. See the GNU General Public License for more details.

## Contact

Contact goodman.maurice@gmail.com if you have any questions pertaining to the use of the functions in this package.