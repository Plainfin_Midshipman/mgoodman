% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/resid_fit.R
\name{resid_fit}
\alias{resid_fit}
\alias{resid_fit.lm}
\alias{resid_fit.lmerMod}
\title{resid_fit: ggplot2 residuals vs. fitted plot}
\usage{
resid_fit(model, ...)

\method{resid_fit}{lm}(model, data, cooks_threshold = 0.05)

\method{resid_fit}{lmerMod}(model, data)
}
\arguments{
\item{model}{a linear model object}

\item{...}{other arguments}

\item{data}{data frame}

\item{cooks_threshold}{Points with cooks distances less than the
cooks_threshold will be labeled individually.}
}
\description{
Produce attractive residuals vs. fitted plots using ggplot2.
Function takes a model object and a data frame, and returns
a residuals vs. fitted plot.
}
